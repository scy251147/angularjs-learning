angular.module('greatApp').directive('postLists', ['Posts', function(Posts) {

  return {
    restrict: 'A',
    scope: {},
    controller: function($scope) {
      $scope.posts = Posts.posts;

      $scope.$on('post:added', function() {
        $scope.posts = Posts.posts;
      });
    },
    template: ["<div class='alert alert-success' ng-repeat='post in posts track by $index' role='alert'>Order {{$index}}: {{post}}</div>"].join('')
  }

}]);;